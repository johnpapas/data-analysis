%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe1Fun1(notBurned, burned, tiitxt, nbins)
% fun=Group14Exe1Fun1(notBurned, burned, tiitxt, nbins)
% Group14Exe1Fun1 takes the data of burned and not burned
% areas for one of their indexes. It plots the 2 histograms
% of these indexes (one for each area in the same plot)
% and then calculates and checks if one comes from normal dist
% If not, then it also checks for Poisson dist
% INPUTS
% - notBurned : vector with the data of the index of the not burned areas we want to analyze
% - burned    : vector with the data of the index of the burned areas we want to analyze
% - tiitxt    : string with the name of the index (needed for the plot title)
% - nbins     : number of bins showed at the histogramm
% OUTPUTS
% - fun       : the figure with the histogramms on it


   
    bins=0:nbins-1;
    a=0.05;
    
%Creates the plots

    figure;
    clf
    hist1=histogram(notBurned, nbins);
    hold on
    hist2=histogram(burned, nbins);
    

    h1=chi2gof(notBurned);
    h2=chi2gof(burned);
    
%Checks for normal dist

    if(h1==0)
        fprintf('The %s of the not burned areas comes from a normal distribution\n', tiitxt);
    
    else
        fprintf('The %s of the not burned areas doesnt come from a normal distribution\n', tiitxt);
        
%Checks for Poisson dist
        counts=hist1.Values;
        n1=sum(counts);
        lambda=sum(bins.*counts)/n1;
        expCounts=n1*poisspdf(bins,lambda);
        [h,~]=chi2gof(bins, 'Ctrs', bins, 'Frequency', counts, ...
                            'Alpha', a, 'Expected', expCounts, 'NParams', 1);
        
        if(h==0)
            fprintf('The %s of the not burned areas comes from a poisson distribution\n', tiitxt);
        else
            fprintf('The %s of the not burned areas doesnt come from a poisson distribution\n', tiitxt);
        end
    end
    
%Checks for normal dist

    if(h2==0)
       fprintf("The %s of the burned areas comes from a normal distribution\n", tiitxt);
    else
        fprintf("The %s of the burned areas doesnt come from a normal distribution\n", tiitxt);
%Checks for Poisson dist
        counts=hist2.Values;
    
%         pd = fitdist(bins','Poisson','Frequency',counts');
%         expCounts = length(burned) * pdf(pd,bins);
%     
%         h=chi2gof(bins,'Ctrs',bins,...
%                             'Frequency',counts, ...
%                             'Expected',expCounts,...
%                             'NParams',1);
        n1=sum(counts);
        lambda=sum(bins.*counts)/n1;
        expCounts=n1*poisspdf(bins,lambda);
        [h,~]=chi2gof(bins, 'Ctrs', bins, 'Frequency', counts, ...
                            'Alpha', a, 'Expected', expCounts, 'NParams', 1);
                        
        if(h==0)
	        fprintf('The %s of the burned areas comes from a poisson distribution\n', tiitxt);
        else
            fprintf('The %s of the burned areas doesnt come from a poisson distribution\n', tiitxt);
        end
    end

    title(sprintf('Histogramm of %s', tiitxt));
    ylabel("Frequency of appearance");
    xlabel(sprintf("Value of %s", tiitxt));
    legend("Not Burned", "Burned");



end