%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
nbins=8;
[i]=find(~dataM(:,13));
notBurned=dataM(i,:);
[j]=find(dataM(:,13));
burned=dataM(j,:);
temp=9;
RH=10;
wind=11;


figure(1)
Group14Exe1Fun1(notBurned(:,temp), burned(:,temp), 'temperature', nbins);
figure(2)
Group14Exe1Fun1(notBurned(:,wind), burned(:,wind), 'wind', nbins);
figure(3)
Group14Exe1Fun1(notBurned(:,RH), burned(:,RH), 'relative humidity', nbins);
