%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
[i]=find(~dataM(:,13));
notBurned=dataM(i,:);
[j]=find(dataM(:,13));
burned=dataM(j,:);
temp=9;
RH=10;
wind=11;


Group14Exe2Fun1(notBurned(:,temp), burned(:,temp), 'temperature');
Group14Exe2Fun1(notBurned(:,wind), burned(:,wind), 'wind');
Group14Exe2Fun1(notBurned(:,RH), burned(:,RH), 'relative humidity');


%We can see that the means of the temperature from the burned and not burned 
%areas may be the same. With the little samples we see that the probability 
%is about 95%. With the initial samples, with a significance level of 5%
%the null hypothesis for difference of the means equals 0, is accepted. So
%the 2 answers agree.
%We can see that the means of the wind from the burned and not burned 
%areas may be the same. With the little samples we see that the probability 
%is about 95%. With the initial samples, with a significance level of 5%
%the null hypothesis for difference of the means equals 0, is accepted. So
%the 2 answers agree.
%We can see that the means of the RH from the burned and not burned 
%areas may be the same. With the little samples we see that the probability 
%is about 95%. With the initial samples, with a significance level of 5%
%the null hypothesis for difference of the means equals 0, is accepted. So
%the 2 answers agree.
%In all of the occasions, the confidence interval is different because with
%the small samples we have less accuracy, and that's why they are wider
%than the initial samples