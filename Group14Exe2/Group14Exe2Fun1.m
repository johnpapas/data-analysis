%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe2Fun1(notBurned, burned, tiitxt)
% fun=Group14Exe2Fun1(notBurned, burned, tiitxt)
% Group14Exe1Fun1 takes the data of burned and not burned
% areas for one of their indexes. It checks if 
% these indexes (one for each area in the same plot)
% have the same mean (Null hypothesis for difference of
% means equals 0)
% INPUTS
% - notBurned : vector with the data of the index of the not burned areas we want to analyze
% - burned    : vector with the data of the index of the burned areas we want to analyze
% - tiitxt    : string with the name of the index (needed for the prints)
% OUTPUTS
% - fun       : prints if they may or may not have same mean and the
%               confidence intervals

    
    k=length(notBurned);
    l=length(burned);
    M=50;
    n=20;
    samplesNB=zeros(M,n);
    samplesB=zeros(M,n);
    j=0;
    ciMeanSamp=zeros(M,2);
    
    %Creates the 50 samples of 20 obs
    for i=1:M
        samplesNB(i,:)=notBurned(randperm(k,n));
        samplesB(i,:)=burned(randperm(l,n));
    end
    
    %Confidence interval of our first samples
    [~,~,ciMean,~]=ttest2(notBurned, burned);
    if(ciMean(1)*ciMean(2)<0)
        fprintf("The %s may have the same mean at both the burned and not burned areas\n", tiitxt);
    end
    
    %Confidence interval of the little samples
    for iM=1:M
        [~,~,ciMeanSamp(iM,1:2),~]=ttest2(samplesNB(iM,:), burned(iM,:));
        if(ciMeanSamp(iM,1)*ciMeanSamp(iM,2)<0)
            j=j+1;
        end
    end
    fprintf("The %s may have the same mean at both burned and not burned areas with a probability of %d\n", tiitxt, j*100/M);
    fprintf("The confidence interval for the initial samples is [%f,%f]. ", ciMean(1), ciMean(2));
    fprintf("The mean confidence interval for the small samples is [%f,%f]. \n", mean(ciMeanSamp(:,1)), mean(ciMeanSamp(:,2)));
    

end