%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe3Fun1(notBurned, burned,tiitxt)
% fun=Group14Exe3Fun1(notBurned, burned, tiitxt)
% Group14Exe1Fun1 takes the data of burned and not burned
% areas for one of their indexes. It checks if 
% these indexes (one for each area in the same plot)
% have the same median (Null hypothesis for difference of
% means equals 0) with bootstrap
% INPUTS
% - notBurned : vector with the data of the index of the not burned areas we want to analyze
% - burned    : vector with the data of the index of the burned areas we want to analyze
% - tiitxt    : string with the name of the index (needed for the prints)
% OUTPUTS
% - fun       : prints if they may or may not have same median and the
%               confidence intervals

    k=length(notBurned);
    l=length(burned);
    M=50;
    n=20;
    B=1000;
    a=0.05;
    samplesNB=zeros(M,n);
    samplesB=zeros(M,n);
	medianNB=median(notBurned);
    medianB=median(burned);
    ciMedianSamp=zeros(M,2);
    counter=0;

    %Creates the 50 samples of 20 obs
    for i=1:M
        samplesNB(i,:)=notBurned(randperm(length(notBurned),n));
        samplesB(i,:)=burned(randperm(length(burned),n));
    end
    
    %Creates bootstrap samples, finds the median and sorts the vector to
    %see if the real median is in between the B*(a/2) and B*(1-a/2) indexes
    %of the vector
    
    klower = floor((B+1)*a/2);
    kup = B+1-klower;
    tailpercV = [klower kup]*100/B;
    bootmedianDiff=zeros(B,1);
    for iB=1:B
        bootmedianDiff(iB)=median(notBurned(unidrnd(k,k,1)))-median(burned(unidrnd(l,l,1)));
    end
    ciMedian(1:2)=prctile(bootmedianDiff,tailpercV);
    if(ciMedian(1)*ciMedian(2)<0)
        fprintf("The %s may have the same median at both the burned and not burned areas\n", tiitxt);
    else
        fprintf("The %s may not have the same median at both the burned and not burned areas\n", tiitxt);
    end
    fprintf("The confidence interval of the median difference is [%f, %f]\n", ciMedian(1), ciMedian(2));
    
    %Little Samples
    %Does the same as before but for all the small samples
    
    klower = floor((B+1)*a/2);
    kup = B+1-klower;
    tailpercV = [klower kup]*100/B;
    bootmedianDiff=zeros(B,1);
    for iM=1:M
        for iB=1:B
            bootmedianDiff(iB)=median(samplesNB(iM, unidrnd(n,n,1)))-median(samplesB(iM, unidrnd(n,n,1)));
        end
        ciMedianSamp(iM,1:2)=prctile(bootmedianDiff,tailpercV);
        if(ciMedianSamp(iM,1)*ciMedianSamp(iM,2)<0)
            counter=counter+1; 
        end
    end
    
    fprintf("The %s may have the same median at both the burned and not burned area at %d %% \n", tiitxt, counter*100/M);
    fprintf("The confidence interval of the median difference is [%f, %f]\n", mean(ciMedianSamp(:,1)), mean(ciMedianSamp(:,2)));
    
        
end