%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
[i]=find(~dataM(:,13));
notBurned=dataM(i,:);
[j]=find(dataM(:,13));
burned=dataM(j,:);
temp=9;
RH=10;
wind=11;


Group14Exe3Fun1(notBurned(:,temp), burned(:,temp), 'temperature')
Group14Exe3Fun1(notBurned(:,wind), burned(:,wind), 'wind')
Group14Exe3Fun1(notBurned(:,RH), burned(:,RH), 'relative humidity')
