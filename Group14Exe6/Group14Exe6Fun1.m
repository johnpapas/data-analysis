%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function [b0, b1, b1ci, b0ci]=Group14Exe6Fun1(index1V, index2V)
%[b0, b1, b1ci, b0ci]=Group14Exe6Fun1(index1V, index2V)
%Function that calculates the confidence interval and the real value of the slope and y-intercept 
%of the linear regression with the parametric method. The y-intercept is
%not needed at this exercise.
% INPUTS
% - index1V   : vector with the data of one index 
% - index2V   : vector with the data of another index 
% OUTPUTS
% - b1ci      : the confidence interval of the slope, as a vector 
% - b0ci      : the confidence interval of the y-intercept, as a vector 
% - b1        : the real value of the slope
% - b0        : the real value of the y-intercept

    a=0.05;
    n=length(index1V);

    b1=sum((index1V-mean(index1V)).*(index2V-mean(index2V)))/sum((index1V-mean(index1V)).*(index1V-mean(index1V)));
    b0=mean(index2V)-b1*mean(index1V);
    x=index1V;
    y=b0+b1*x;


    sx=(sum((index1V-mean(index1V)).*(index1V-mean(index1V))))/(n-1);
    sxy=sum((index1V-mean(index1V)).*(index2V-mean(index2V)))/(n-1);
    sy=sum((index2V-mean(index2V)).*(index2V-mean(index2V)))/(n-1);
    Sxx=sx*(n-1);

    se=sqrt(sy*(n-1)/(n-2));
    sb1=se/sqrt(Sxx);

    t=tinv((1-(a/2)),n-2);

    b1ci(1)=b1-t*sb1;
    b1ci(2)=b1+t*sb1;

    b0ci(1)=b0-t*se*sqrt((1/n)+(mean(index1V)/(Sxx)));
    b0ci(2)=b0+t*se*sqrt((1/n)+(mean(index1V)/(Sxx)));

    
end
