%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
temp=9;
RH=10;
M=100;
n=40;
counterP=0;
counterB=0;

[~,b1real,~,~]=Group14Exe6Fun1(dataM(:,temp), dataM(:,RH));

samplesM=zeros(M,n,2);
b1=zeros(M,1);
b1ci=zeros(M,2);
b1ciboots=zeros(M,2);

for iM=1:M
    r=unidrnd(length(dataM(:,1)),n,1);
    samplesM(iM,:,1)=dataM(r,temp);
    samplesM(iM,:,2)=dataM(r,RH);
    [~, b1(iM), b1ci(iM,:), ~]=Group14Exe6Fun1(samplesM(iM,:,1), samplesM(iM,:,2));
    b1ciboots(iM,:)=Group14Exe6Fun2(samplesM(iM,:,1), samplesM(iM,:,2),1000);
end

figure(1)
clf
histogram(b1(:))
hold on
line([b1real b1real], [0 30],'Color','red','LineStyle','--')
title("Histogram of the parametric check for the slope")
ylabel("Frequency of appearance");
xlabel("Value of slope");

for iM=1:M
    if (b1real<b1ci(iM,2) && b1real>b1ci(iM,1))
        counterP=counterP+1;
    end
    if (b1real<b1ciboots(iM,2) && b1real>b1ciboots(iM,1))
        counterB=counterB+1;
    end
end
fprintf("The real slope (%f) is accepted %d %% in the parametric ci and %d %% in the bootstrap ci\n", b1real, counterP*100/M, counterB*100/M);

%The percentages of the parametric and the bootstrap check are almost the
%same(if not the same). The percentages are at a rate of 95 because we use
%significance level 0.05

