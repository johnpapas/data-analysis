%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function b1ci=Group14Exe6Fun2(index1V, index2V, B)
%b1ci=Group14Exe6Fun2(index1V, index2V, B)
%Function that calculates the confidence interval of the slope of the 
%linear regression with the bootstrap method
% INPUTS
% - index1V   : vector with the data of one index 
% - index2V   : vector with the data of another index 
% - B         : number of bootstrap samples we will use
% OUTPUTS
% - b1ci      : the confidence interval of the slope, as a vector 

    n=length(index1V);
    samplesM=zeros(B,n,2);
    b1=zeros(B,1);
    a=0.05;
    for iB=1:B
        r=unidrnd(n,n,1);
        samplesM(iB,:,1)=index1V(r);
        samplesM(iB,:,2)=index2V(r);
        [~,b1(iB),~,~]=Group14Exe6Fun1(samplesM(iB,:,1),samplesM(iB,:,2));
    end
    b1V=sort(b1);
    b1ci=[b1V(B*a/2) b1V(B*(1-(a/2)))];
    
end





