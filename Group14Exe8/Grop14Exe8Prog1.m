%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
M=100;
n=40;
windV=dataM(:,11);

%The inmodel, returns a logical vector with the variables used at the final
%form having logical 1.
[~,~,~,inmodel]=stepwisefit(dataM(:,[1:10 12:13]),windV(:));
x0V=inmodel;

%Little samples
xIM=NaN(M,length(dataM(1,:))-1);

for iM=1:M
    r=randperm(length(dataM(:,1)),n);
    [~,~,~,inmodel]=stepwisefit(dataM(r,[1:10 12:13]),windV(r));
    xIM(iM,:)=inmodel;
end

xSum=sum(xIM(:,x0V));%Counting how many times the initial used variables are used at the small samples method

fprintf("The frequency of appearance of the variables, used at the initial sample, at the small samples is [");
fprintf("%g ", xSum);
fprintf("]");

%By running/repeating the code many times, we can see that one variable is
%used at a rate of almost 25%(the 5th one) while another variable is used
%at a rate of 5%. That means that the first variable is used a lot while
%the second one not to much