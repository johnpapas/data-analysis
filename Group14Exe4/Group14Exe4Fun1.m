%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe4Fun1(index1V, index2V, txt1, txt2)
% fun=Group14Exe4Fun1(index1V, index2V, txt1, txt2)
% Function that checks if the two indexes have a correlation with
% parametric check.
% INPUTS
% - index1V   : vector with the data of one index 
% - index2V   : vector with the data of another index 
% - txt1      : string with the name of the one index (needed for the prints)
% - txt2      : string with the name of the other index (needed for the prints)
% OUTPUTS
% - fun       : prints if the 2 indexes may have a correlation 


    [~,~,RL,RU]=corrcoef(index1V, index2V);
    if(RL(1,2)*RU(1,2)<0)
        fprintf("The %s and the %s are not related\n", txt1, txt2);
    else
        fprintf("The %s and the %s may be related\n", txt1, txt2);
    end

end