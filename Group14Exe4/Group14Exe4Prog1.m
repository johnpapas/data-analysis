%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
r=unidrnd(length(dataM(:,1)),40,1);
sampleM=dataM(r,:);
names=["FFMC", "DMC", "DC", "ISI", "temp", "RH", "wind"];


for i=5:11
    for j=i:11
        if i==j
            continue;
        end
        Group14Exe4Fun1(sampleM(:,i), sampleM(:,j), names(i-4), names(j-4))
    end
end

for i=5:11
    for j=i:11
        if i==j
            continue;
        end
        Group14Exe4Fun2(sampleM(:,i), sampleM(:,j), names(i-4), names(j-4))
    end
end

%The 2 types of checking agree for all the pair of indexes almost every
%time