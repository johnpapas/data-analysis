%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe4Fun2(index1V, index2V, txt1, txt2)
% fun=Group14Exe4Fun2(index1V, index2V, txt1, txt2)
% Function that checks if the two indexes have a correlation with bootstrap
% method.
% INPUTS
% - index1V   : vector with the data of one index 
% - index2V   : vector with the data of another index 
% - txt1      : string with the name of the one index (needed for the prints)
% - txt2      : string with the name of the other index (needed for the prints)
% OUTPUTS
% - fun       : prints if the 2 indexes may have a correlation 

    
    indexBoth=[index1V index2V];
    B=1000;
    n=length(index1V);
    a=0.05;
    
    %Creates the bootstrap samples from the initial samples and checks if
    %the real correlation coefficient is in between the bootstraps sorted
    %vector of correlation coefficients (B*(a/2),B*(1-a/2))
    
    tmp=corrcoef(indexBoth);
    r0=tmp(1,2);
    rV=zeros(B,1);
    for i=1:B
        newIndexBoth=[index1V index2V(randperm(n))];
        tmp=corrcoef(newIndexBoth);
        rV(i)=tmp(1,2);
    end
    
    tV=zeros(B,1);
    t0=r0*sqrt((n-2)/(1-r0^2));
    for i=1:B
        tV(i)=rV(i)*sqrt((n-2)/(1-(rV(i)^2)));
    end
    
    tV=sort(tV);
    
    lower=B*a/2;
    upper=B*(1-a/2);
    
    if(tV(lower)<t0 && tV(upper)>t0)
        fprintf("The %s and %s are not be related \n", txt1, txt2);
    else
        fprintf("The %s and the %s may be related\n", txt1, txt2);
    end
end
