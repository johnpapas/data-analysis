%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
B=1000;
n=length(dataM(:,1));
k=length(dataM(1,:));
dBoot=0;

eV1=Group14Exe9Fun1(dataM);
figure(1)
clf
plot(sort(eV1,'descend'),'-o')
hold on
line([0 k],[mean(eV1) mean(eV1)], 'Color','red','LineStyle','--')
title("Scree plot")
xlabel('index')
ylabel('eigenvalue')

dScree=sum(eV1>mean(eV1));


tmp=dataM(:,:);

%Bootstrap
eM=zeros(B,length(dataM(1,:)));
for iB=1:B
    for iK=1:k
        r=randperm(n,n);
        tmp(:,iK)=dataM(r,iK);
    end
    eM(iB,:)=Group14Exe9Fun1(tmp);
end

    
for iK=1:k
    dBoot=dBoot+Group14Exe9Fun2(eM(:,iK), eV1(iK));
end

fprintf("The dimension from the scree plot is %d and from the bootstrap method %d\n",dScree, dBoot);