%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe9Fun1(dataM)

p=length(dataM(1,:));
n=length(dataM(:,1));

%Centralizing
for iP=1:p
    dataM(:,iP)=dataM(:,iP)-mean(dataM(:,iP));
    dataM(:,iP)=dataM(:,iP)/std(dataM(:,iP));
end

S=(1/(n-1))*(dataM')*dataM;
e=eig(S); %eigenvalues
[A,~]=eig(S); %eigenvectors
L=A'*S*A;


fun=e;

end