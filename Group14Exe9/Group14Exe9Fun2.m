%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe9Fun2(eV, e)

n=length(eV);
a=0.05;
eV=sort(eV);
if(e>eV(n*(1-a/2)))
    fun=1;
else
    fun=0;
end

end