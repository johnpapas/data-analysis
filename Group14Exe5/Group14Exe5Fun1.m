%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

function fun=Group14Exe5Fun1(index1V, index2V, txt1, txt2)
%fun=Group14Exe5Fun1(index1V, index2V, txt1, txt2)
%Function that finds the two factors for the linear regression, plots the
%samples with the regression line and also calculates the errors and plots
%the Scatter Error Plot.
% INPUTS
% - index1V   : vector with the data of one index 
% - index2V   : vector with the data of another index 
% - txt1      : string with the name of the one index (needed for the plots)
% - txt2      : string with the name of the other index (needed for the plots)
% OUTPUTS
% - fun       : prints the correlation coefficient of the 2 indexes and
%               plots the regression line and the scatter error plot

n=length(index1V);

%Linear regression calculation

b1=sum((index1V-mean(index1V)).*(index2V-mean(index2V)))/sum((index1V-mean(index1V)).*(index1V-mean(index1V)));
b0=mean(index2V)-b1*mean(index1V);
x=index1V;
y=b0+b1*x;
figure
clf
plot(index1V,index2V,'.');
hold on;
plot(x,y);
xlabel(txt1)
ylabel(txt2)
title(sprintf('Linear relation between %s and %s', txt1, txt2));

%Correlation coefficient calculation

r=corrcoef([index1V index2V]);
fprintf("The coefficient of %s %s determination is %f\n", txt1, txt2, r(1,2)^2);

%Plotting the scatter error plot

eV=b0+(b1*index1V)-index2V; %Error Vector
sy=sum((index2V-mean(index2V)).*(index2V-mean(index2V)))/(n-1);
se=sqrt(sy*(n-1)/(n-2));
eiV=eV./se;

figure
clf
scatter(b0+(b1*index1V),eiV);
hold on
title(sprintf("Scatter of errors of %s and %s", txt1, txt2))
line([min(b0+(b1*index1V)) max(b0+(b1*index1V))], [1.96 1.96], 'Color','red','LineStyle','--' )
hold on
line([min(b0+(b1*index1V)) max(b0+(b1*index1V))], [-1.96 -1.96], 'Color','red','LineStyle','--')


end
