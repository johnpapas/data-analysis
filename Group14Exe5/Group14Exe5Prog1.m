%Data Analysis Project 2019-2020
%Papas Ioannis
%9218

dataM=importdata('forestfires.dat');
n=40;
r=unidrnd(length(dataM(:,1)),n,1);
sampleM=dataM(r,:);
alpha=0.05;
temp=9;
RH=10;
wind=11;

figure(1)
Group14Exe5Fun1(sampleM(:,temp), sampleM(:,RH), 'Temperature', 'Relative humidity');
figure(2)
Group14Exe5Fun1(sampleM(:,temp), sampleM(:,wind), 'Temperature', 'Wind');

%We can see that the 2 results are a bit different. From the scatter error plot we can see that the linear
%regression is better fitted at the temperature and relative humidity model
%Also we can't say, that one of the models may be better fitted from a
%polynomial regression.


